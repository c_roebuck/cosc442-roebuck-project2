/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */
public class VendingMachineTest {
	VendingMachine foobar;
	VendingMachineItem soda;
	/**
	 * @throws java.lang.Exception
	 */
	
	@Before
	public void setUp() throws Exception {
		VendingMachine foobar = new VendingMachine();
		VendingMachineItem soda = new VendingMachineItem("soda", 10.0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		VendingMachine foobar = null;
		VendingMachineItem soda = null;
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#VendingMachine()}.
	 */
	@Test
	public void testVendingMachine() {
		
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#addItem(edu.towson.cis.cosc442.project2.vendingmachine.VendingMachineItem, java.lang.String)}.
	 */
	@Test
	//testing if items can be added successfully to the vending machine
	public void testAddItem() {
		foobar.addItem(soda, "4");
		foobar.getItem("4");
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#removeItem(java.lang.String)}.
	 */
	@Test
	//testing if items can be successfully removed from the vending machine
	public void testRemoveItem() {
		foobar.removeItem("4");
		foobar.getItem("4");
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#insertMoney(double)}.
	 */
	@Test
	//testing if money can be inserted into the vending machine
	public void testInsertMoney() {
		foobar.insertMoney(3.0);
		double amount = foobar.getBalance();
		assertEquals("3.0", amount);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#getBalance()}.
	 */
	@Test
	//testing if a balance can be found in the vending machine once inserted
	public void testGetBalance() {
		foobar.insertMoney(10.0);
		double amount = foobar.getBalance();
		assertEquals("10.0", amount);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#makePurchase(java.lang.String)}.
	 */
	@Test
	//testing if money and items are extracted from the vending machine
	public void testMakePurchase() {
		foobar.insertMoney(10.0);
		foobar.makePurchase("4");
		foobar.getItem("4");
		double amount = foobar.getBalance();
		assertEquals("0.0", amount);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#returnChange()}.
	 */
	@Test
	//testing if change is returned from the vending machine
	public void testReturnChange() {
		foobar.insertMoney(10.0);
		foobar.returnChange();
		double amount = foobar.getBalance();
		assertEquals("0.0", amount);
	}

}
